# Advanced Programming For Games Project
## Project Overview
### Objective
The objective of this coursework was to build a program in C++ to constuct a simulation of a multi-lock safe

### Notes
It is a command line based program that uses files in the running directory as input/output

The two PDFs in the root directory outline the coursework

Part1.exe will generate files with are then used in Part2.exe

## Build Information
The solution file is in the root directory

Startup project should be set to either **Part 1** or **Part 2**

Build directories are in the root directory, either in **Win32** or **x64** and then either **Release** or **Debug**
