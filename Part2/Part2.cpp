// TheSafeProblem.cpp : Defines the entry point for the console application.
//
#include "pch.h"
#include "stdafx.h"
#include <iostream>;
#include <exception>
#include <fstream>
#include <map>

#include "MultiLockSafe.h"

using namespace std;

string keyFileName;
string lockedSafeFileName;
string newMultiSafeFileName = "multiSafeFilePart2";

map<Dials, vector<Dials>> validSafeMap;
vector<MultiLockSafe> testingSafes;

vector<vector<DialRotations>> validHashFunctions;

SafeSettings& safeSettings = SafeSettings::getInstance();

void readSettingsFile() {
	string line;
	ifstream settingsFile("settings.txt");
	if (settingsFile.is_open()) {
		while (getline(settingsFile, line)) {
			if (line.find("Number of locks per safe = ") != string::npos) {
				line = line.substr(line.find("= ") + 2);
				try {
					int numLocks = stoi(line);
					if (numLocks > 0)
						safeSettings.setNumLocks(numLocks);
					else
						cout << "Number of locks in settings file must be greater than 0." << endl << "Using default value of " << safeSettings.getNumLocks() << endl;
				}
				catch (invalid_argument iae) {
					cout << "Number of locks in the settings file was invalid.\n" <<
						"Using default value of " << safeSettings.getNumLocks() << endl;
				}
			}
		}
	}
	settingsFile.close();
}

void readLockedSafeFile() {
	string line;
	ifstream lockedSafeFile(lockedSafeFileName + ".txt");
	if (lockedSafeFile.is_open()) {
		while (getline(lockedSafeFile, line)) {
			if (line.find("ROOT: ") != string::npos) {
				line = line.substr(line.find(": ") + 2);
				Dials root;
				root.setDials(line[0] - '0', line[1] - '0', line[2] - '0', line[3] - '0');
				vector<Dials> lns;
				validSafeMap.insert(pair<Dials, vector<Dials>>(root, lns));
				while (getline(lockedSafeFile, line)) {
					if (line.find("LN") == string::npos)
						break;
					line = line.substr(line.find(": ") + 2);
					Dials ln;
					if (line.find(" ") == string::npos) {
						ln.setDials(line[0] - '0', line[1] - '0', line[2] - '0', line[3] - '0');
					}
					else {
						ln.setDials(line[0] - '0', line[2] - '0', line[4] - '0', line[6] - '0');
					}
					validSafeMap.at(root).push_back(ln);
				}
			}
		}
	}
	lockedSafeFile.close();
}

void generateSafesFromKeyFile() {
	string line;
	ifstream keyFile(keyFileName + ".txt");
	if (keyFile.is_open()) {
		while (getline(keyFile, line)) {
			if (line.find("ROOT") != string::npos) {
				line = line.substr(line.find("ROOT ") + 5);
				Dials root;
				root.setDials(line[0] - '0', line[1] - '0', line[2] - '0', line[3] - '0');

				if (validSafeMap.find(root) != validSafeMap.end()) {
					MultiLockSafe multiSafe;
					multiSafe.setRoot(root);
					getline(keyFile, line);
					if (line.find("UHF") != string::npos) {
						line = line.substr(line.find("UHF ") + 4);
						DialRotations uhf;
						uhf.setRotations(stoi(line.substr(0, 2)), stoi(line.substr(3, 2)), stoi(line.substr(6, 2)), stoi(line.substr(9, 2)));
						multiSafe.setUHF(uhf);
						getline(keyFile, line);
					}
					if (line.find("LHF") != string::npos) {
						line = line.substr(line.find("LHF ") + 4);
						DialRotations lhf;
						lhf.setRotations(stoi(line.substr(0, 2)), stoi(line.substr(3, 2)), stoi(line.substr(6, 2)), stoi(line.substr(9, 2)));
						multiSafe.setLHF(lhf);
						getline(keyFile, line);
					}
					if (line.find("PHF") != string::npos) {
						line = line.substr(line.find("PHF ") + 4);
						DialRotations phf;
						phf.setRotations(stoi(line.substr(0, 2)), stoi(line.substr(3, 2)), stoi(line.substr(6, 2)), stoi(line.substr(9, 2)));
						multiSafe.setPHF(phf);
					}
					multiSafe.processSafe();
					testingSafes.push_back(multiSafe);
				}
			}
		}
	}
	keyFile.close();
}

void generateMultiSafeFile() {
	ofstream multiSafeFile(newMultiSafeFileName + ".txt");

	if (multiSafeFile.is_open()) {
		int ns = 0;
		for (int i = 0; i < testingSafes.size(); ++i) {
			for (int j = 0; j < validHashFunctions.size(); ++j) {
				multiSafeFile << "NS " << ns << " ";
				++ns;
				multiSafeFile << "VALID" << endl;
				MultiLockSafe safe(testingSafes.at(i).getRoot(), validHashFunctions.at(j).at(0), validHashFunctions.at(j).at(1), validHashFunctions.at(j).at(2));
				safe.processSafe();
				multiSafeFile << safe << endl;
			}
		}
	}
	multiSafeFile.close();
}

bool isValidFilename(string filename) {
	if (filename.find('\\') == string::npos &&
		filename.find('/') == string::npos &&
		filename.find('<') == string::npos &&
		filename.find('>') == string::npos &&
		filename.find('|') == string::npos &&
		filename.find('"') == string::npos &&
		filename.find(':') == string::npos &&
		filename.find('?') == string::npos &&
		filename.find('*') == string::npos)
		return true;
	return false;
}



bool fileExists(string filename) {
	ifstream testFile(filename + ".txt");
	return testFile.good();
}

bool isAKeyFile(string filename) {
	string line;
	ifstream keyFile(filename + ".txt");
	if (keyFile.is_open()) {
		getline(keyFile, line);
		if (line.find("NS") != string::npos) {
			getline(keyFile, line);
			if (line.find("ROOT") != string::npos) {
				getline(keyFile, line);
				if (line.find("UHF") != string::npos) {
					getline(keyFile, line);
					if (line.find("LHF") != string::npos) {
						getline(keyFile, line);
						if (line.find("PHF") != string::npos) {
							return true;
						}
					}
				}
			}
		}
	}
	return false;
}

bool isALockedSafeFile(string filename) {
	string line;
	ifstream keyFile(filename + ".txt");
	if (keyFile.is_open()) {
		getline(keyFile, line);
		if (line.find("NL") != string::npos) {
			getline(keyFile, line);
			if (line.find("ROOT") != string::npos) {
				getline(keyFile, line);
				if (line.find("LN") != string::npos) {
					return true;
				}
			}
		}
	}
	return false;
}

int main()
{
	/* Start user input */
	while (true) {
		cout << "Enter the filename of an existing key file: ";
		getline(cin, keyFileName);
		if (fileExists(keyFileName))
			if(isAKeyFile(keyFileName))
				break;
		cout << "File does not exist or is not formatted correctly!" << endl;
	}

	while (true) {
		cout << "Enter the filename of an existing locked safe file: ";
		getline(cin, lockedSafeFileName);
		if (fileExists(lockedSafeFileName) && lockedSafeFileName != keyFileName)
			if(isALockedSafeFile(lockedSafeFileName))
				break;
		cout << "File does not exist or is not formatted correctly!" << endl;
	}

	while (true) {
		string tempFilename = "";
		cout << "Enter a filename for the new multi safe file: ";
		getline(cin, newMultiSafeFileName);
		if (isValidFilename(tempFilename) && tempFilename != keyFileName) {
			if (tempFilename != "")
				newMultiSafeFileName = tempFilename;
			break;
		}
		cout << "Invalid filename!" << endl;
	}

	/* Get data from files */

	readSettingsFile();
	readLockedSafeFile();
	generateSafesFromKeyFile();

	DialRotations uhf;
	DialRotations lhf;
	DialRotations phf;

	/* Calculate valid hash functions */

	for (int i = 0; i < 9999; ++i) {
		int x = (i / 1000) % 10;
		int y = (i / 100) % 10;
		int z = (i / 10) % 10;
		int w = i % 10;
		Dials cn(x, y, z, w);

		if (cn.isDialValid()) {
			Dials root = validSafeMap.begin()->first;
			uhf = cn - root;
			Dials ln = validSafeMap.find(root)->second.at(0);
			lhf = ln - cn;
			Dials ln2 = validSafeMap.find(root)->second.at(1);
			phf = (ln2 - ln) - (ln - root);

			MultiLockSafe safe(root, uhf, lhf, phf);
			safe.processSafe();

			if (safe.isSafeValid()) {
				bool allSafesValid = true;
				for (auto const& mapIt : validSafeMap) {
					MultiLockSafe testSafe(mapIt.first, uhf, lhf, phf);
					testSafe.processSafe();
					if (!testSafe.isSafeValid())
						allSafesValid = false;
				}
				if (allSafesValid) {
					vector<DialRotations> hashes;
					hashes.push_back(uhf);
					hashes.push_back(lhf);
					hashes.push_back(phf);
					validHashFunctions.push_back(hashes);
				}
			}
		}
	}
	cout << "Number of candidate valid hash functions: " << validHashFunctions.size() << endl;

	/* Generate a multisafe file using the valid hash functions */

	generateMultiSafeFile();

	/* Print the correct hash function if the user requests */
	bool findSolution = false;
	while (true) {
		string input = "";
		cout << "Would you like to use the hash functions on safes generated by the key file to find the correct hash functions? (y/n)";
		getline(cin, input);
		if (input == "y") {
			findSolution = true;
			break;
		}
		else if (input == "n")
			break;
	}

	if (findSolution) {
		for (int i = 0; i < validHashFunctions.size(); ++i) {
			bool allSafesCorrect = true;
			for (int j = 0; j < testingSafes.size(); ++j) {
				for (int k = 0; k < safeSettings.getNumLocks(); ++k) {
					testingSafes.at(j).getLock(k).rotateNumbers(-validHashFunctions.at(i).at(1));
					if (!testingSafes.at(j).getLock(k).pressButton())
						allSafesCorrect = false;
					testingSafes.at(j).getLock(k).resetLock();
				}
			}
			if (allSafesCorrect)
				cout << "All safes were valid and correct with the following hashes\n" <<
				"UHF: " << validHashFunctions.at(i).at(0) << endl <<
				"LHF: " << validHashFunctions.at(i).at(1) << endl <<
				"PHF: " << validHashFunctions.at(i).at(2) << endl;
		}
	}

	system("pause");

	return 0;
}

