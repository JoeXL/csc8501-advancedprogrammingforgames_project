#pragma once
#include <iostream>
#include "SafeSettings.h"

using namespace std;

class DialRotations
{
public:
	DialRotations();
	DialRotations(const DialRotations& other);
	DialRotations(int x, int y, int z, int w);
	~DialRotations();

	void setRotations(int x, int y, int z, int w);

	DialRotations operator-();

	friend DialRotations operator-(const DialRotations& lhs, const DialRotations& rhs);

	DialRotations& operator=(const DialRotations& other);

	const int& operator[](int i) const;

	friend ostream& operator<<(ostream& ostr, const DialRotations& dialRot);

	int x;
	int y;
	int z;
	int w;

	SafeSettings& dialSettings = SafeSettings::getInstance();
};

