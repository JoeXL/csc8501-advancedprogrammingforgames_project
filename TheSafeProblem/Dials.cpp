#include "stdafx.h"
#include "Dials.h"


Dials::Dials()
{
	x = 0;
	y = 0;
	z = 0;
	w = 0;
}

Dials::Dials(const Dials& other) {
	*this = other;
}

Dials::Dials(int x, int y, int z, int w) {
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
}


Dials::~Dials()
{
}

void Dials::setDials(int x, int y, int z, int w) {
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
	alignValuesIntoRange();
}

const bool& Dials::isDialValid() const {
	if (x == y || x == z || x == w || y == z || y == w || z == w)
		return false;
	return true;
}

Dials operator+(const Dials& lhs, const DialRotations& rhs) {
	Dials result;
	result.x = lhs.x + rhs.x;
	result.y = lhs.y + rhs.y;
	result.z = lhs.z + rhs.z;
	result.w = lhs.w + rhs.w;
	result.alignValuesIntoRange();
	return result;
}

DialRotations operator-(const Dials& lhs, const Dials& rhs) {
	DialRotations rot;
	rot.x = (lhs.x - rhs.x) % (lhs.dialSettings.getMaxRot() + 1);
	rot.y = (lhs.y - rhs.y) % (lhs.dialSettings.getMaxRot() + 1);
	rot.z = (lhs.z - rhs.z) % (lhs.dialSettings.getMaxRot() + 1);
	rot.w = (lhs.w - rhs.w) % (lhs.dialSettings.getMaxRot() + 1);
	return rot;
}

Dials& Dials::operator+=(const DialRotations& rhs) {
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;
	w += rhs.w;
	alignValuesIntoRange();
	return *this;
}

Dials& Dials::operator=(const Dials& other) {
	if (this != &other) {
		this->x = other.x;
		this->y = other.y;
		this->z = other.z;
		this->w = other.w;
	}
	return *this;
}

const bool& Dials::operator==(const Dials& rhs) const {
	if (x == rhs.x && y == rhs.y && z == rhs.z && w == rhs.w)
		return true;
	return false;
}

const int& Dials::operator[](int i) const {
	if (i < 0)
		i = i * -1;
	i = i % 4;
	switch (i) {
	case 0:
		return x;
	case 1:
		return y;
	case 2:
		return z;
	case 3:
		return w;
	}
}

const bool& Dials::operator<(const Dials& rhs) const {
	int i1 = (x * 1000) + (y * 100) + (z * 10) + w;
	int i2 = (rhs.x * 1000) + (rhs.y * 100) + (rhs.z * 10) + rhs.w;
	return i1 < i2;
}

ostream& operator<<(ostream& ostr, const Dials& dial) {
	ostr << dial.x << dial.y << dial.z << dial.w;
	return ostr;
}

void Dials::alignValuesIntoRange() {
	int range = dialSettings.getMaxDial() - dialSettings.getMinDial() + 1;
	if (x < dialSettings.getMinDial())
		x = (x % range) + range;
	else if (x > dialSettings.getMaxDial())
		x = x % range;

	if (y < dialSettings.getMinDial())
		y = (y % range) + range;
	else if (y > dialSettings.getMaxDial())
		y = y % range;

	if (z < dialSettings.getMinDial())
		z = (z % range) + range;
	else if (z > dialSettings.getMaxDial())
		z = z % range;

	if (w < dialSettings.getMinDial())
		w = (w % range) + range;
	else if (w > dialSettings.getMaxDial())
		w = w % range;
}