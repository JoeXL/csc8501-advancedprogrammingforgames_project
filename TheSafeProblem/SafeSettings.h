#pragma once

class SafeSettings {
public:
	static SafeSettings& getInstance() {
		static SafeSettings instance;
		return instance;
	}

	SafeSettings(SafeSettings const&) = delete;
	void operator=(SafeSettings const&) = delete;

	const int& getNumSafes() const {
		return numSafes;
	}

	const int& getMinDial() const {
		return minDial;
	}

	const int& getMaxDial() const {
		return maxDial;
	}

	const int& getMaxRot() const {
		return maxRot;
	}

	const int& getNumLocks() const {
		return numLocks;
	}

	void setNumSafes(int num) {
		numSafes = num;
	}

	void setMinDial(int min) {
		minDial = min;
	}

	void setMaxDial(int max) {
		maxDial = max;
	}

	void setMaxRot(int max) {
		maxRot = max;
	}

	void setNumLocks(int num) {
		numLocks = num;
	}

private:
	SafeSettings() {}

	int numSafes = 100;
	int minDial = 0;
	int maxDial = 9;
	int maxRot = 9;
	int numLocks = 5;
};