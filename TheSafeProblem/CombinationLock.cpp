#include "stdafx.h"
#include "CombinationLock.h"


CombinationLock::CombinationLock() {
	lockIndex = 0;
	isLocked = false;
}

CombinationLock::CombinationLock(const CombinationLock& other) {
	*this = other;
}

CombinationLock::~CombinationLock() {
}

void CombinationLock::rotateNumbers(const DialRotations& rot) {
	currentNumber += rot;
}

const Dials& CombinationLock::getCurrentNumber() const {
	return currentNumber;
}

void CombinationLock::setCurrentNumber(const Dials& dials) {
	currentNumber = dials;
}

bool CombinationLock::pressButton() {
	if (currentNumber == correctNumber) {
		isLocked = false;
		return true;
	}
	return false;
}

void CombinationLock::resetLock() {
	currentNumber = lockNumber;
}

const Dials& CombinationLock::setupLock(const Dials& root, const DialRotations& UHF, const DialRotations& LHF, const DialRotations& PHF) {
	if (!isLocked) {
		correctNumber = root + UHF;
		lockNumber = correctNumber + LHF;
		hashNumber = lockNumber + PHF;
		currentNumber = lockNumber;
		isLocked = true;
	}
	return hashNumber;
}

bool CombinationLock::isLockValid() {
	if (!isLocked || correctNumber[0] == correctNumber[1] || correctNumber[0] == correctNumber[2] || correctNumber[0] == correctNumber[3] || correctNumber[1] == correctNumber[2] || correctNumber[1] == correctNumber[3] || correctNumber[2] == correctNumber[3])
		return false;

	return true;
}

CombinationLock& CombinationLock::operator=(const CombinationLock& other) {
	if (this != &other) {
		this->hashNumber = other.hashNumber;
		this->correctNumber = other.correctNumber;
		this->lockNumber = other.lockNumber;
		this->currentNumber = other.currentNumber;
		this->lockIndex = other.lockIndex;
		this->isLocked = other.isLocked;
	}
	return *this;
}

ostream& operator<<(ostream& ostr, const CombinationLock& comboLock) {

	ostr << "CN" << comboLock.lockIndex << " " << comboLock.correctNumber << ", LN" << comboLock.lockIndex << " " << comboLock.lockNumber << ", HN" << comboLock.lockIndex << " " << comboLock.hashNumber;

	return ostr;
}