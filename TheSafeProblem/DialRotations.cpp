#include "stdafx.h"
#include "DialRotations.h"


DialRotations::DialRotations() {
}

DialRotations::DialRotations(const DialRotations& other) {
	*this = other;
}

DialRotations::DialRotations(int x, int y, int z, int w) {
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
}

DialRotations::~DialRotations()
{
}

void DialRotations::setRotations(int x, int y, int z, int w) {
	this->x = x % (dialSettings.getMaxRot() + 1);
	this->y = y % (dialSettings.getMaxRot() + 1);
	this->z = z % (dialSettings.getMaxRot() + 1);
	this->w = w % (dialSettings.getMaxRot() + 1);
}

DialRotations DialRotations::operator-() {
	DialRotations rot;
	rot.x = -x;
	rot.y = -y;
	rot.z = -z;
	rot.w = -w;
	return rot;
}

DialRotations operator-(const DialRotations& lhs, const DialRotations& rhs) {
	DialRotations rot;
	rot.x = (lhs.x - rhs.x) % (lhs.dialSettings.getMaxRot() + 1);
	rot.y = (lhs.y - rhs.y) % (lhs.dialSettings.getMaxRot() + 1);
	rot.z = (lhs.z - rhs.z) % (lhs.dialSettings.getMaxRot() + 1);
	rot.w = (lhs.w - rhs.w) % (lhs.dialSettings.getMaxRot() + 1);
	return rot;
}

DialRotations& DialRotations::operator=(const DialRotations& other) {
	if (this != &other) {
		this->x = other.x;
		this->y = other.y;
		this->z = other.z;
		this->w = other.w;
	}
	return *this;
}

const int& DialRotations::operator[](int i) const {
	if (i < 0)
		i = i * -1;
	i = i % 4;

	if (i == 0)
		return x;
	if (i == 1)
		return y;
	if (i == 2)
		return z;
	return w;
}

ostream& operator<<(ostream& ostr, const DialRotations& dialRot) {
	ostr << showpos << dialRot.x << ',' << dialRot.y << ',' << dialRot.z << ',' << dialRot.w << noshowpos;
	return ostr;
}