#include "stdafx.h"
#include "MultiLockSafe.h"


MultiLockSafe::MultiLockSafe()
{
	for (int i = 0; i < safeSettings.getNumLocks(); ++i) {
		CombinationLock lock;
		lock.setLockIndex(i);
		locks.push_back(lock);
	}
}

MultiLockSafe::MultiLockSafe(const MultiLockSafe& other) {
	*this = other;
}

MultiLockSafe::MultiLockSafe(Dials root, DialRotations uhf, DialRotations lhf, DialRotations phf) : MultiLockSafe() {
	setRoot(root);
	setUHF(uhf);
	setLHF(lhf);
	setPHF(phf);
}

MultiLockSafe::~MultiLockSafe()
{
}

CombinationLock& MultiLockSafe::getLock(int i) {
	i = i % locks.size();
	return locks.at(i);
}

void MultiLockSafe::processSafe() {
	Dials initial = root;
	for (int i = 0; i < locks.size(); ++i) {
		initial = locks[i].setupLock(initial, UHF, LHF, PHF);
	}
}

const bool& MultiLockSafe::isSafeValid() const {
	for (CombinationLock lock : locks) {
		if (!lock.isLockValid())
			return false;
	}
	return true;
}

MultiLockSafe& MultiLockSafe::operator=(const MultiLockSafe& other) {
	if (this != &other) {
		this->locks = other.locks;
		this->root = other.root;
		this->UHF = other.UHF;
		this->LHF = other.LHF;
		this->PHF = other.PHF;
	}
	return *this;
}

ostream& operator<<(ostream& ostr, const MultiLockSafe& comboLock) {

	for (CombinationLock cl : comboLock.locks) {
		ostr << cl << endl;
	}

	return ostr;
}