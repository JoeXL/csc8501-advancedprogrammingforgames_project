#pragma once
#include "CombinationLock.h"

class MultiLockSafe
{
public:
	MultiLockSafe();
	MultiLockSafe(const MultiLockSafe& other);
	MultiLockSafe(Dials root, DialRotations uhf, DialRotations lhf, DialRotations phf);
	~MultiLockSafe();

	void setRoot(Dials newRoot) {
		root = newRoot;
	}

	void setUHF(const DialRotations& rot) {
		UHF = rot;
	}

	void setLHF(const DialRotations& rot) {
		LHF = rot;
	}

	void setPHF(const DialRotations& rot) {
		PHF = rot;
	}

	const Dials& getRoot() const {
		return root;
	}

	CombinationLock& getLock(int i);

	void processSafe();
	const bool& isSafeValid() const;

	MultiLockSafe& operator=(const MultiLockSafe& other);

	friend ostream& operator<<(ostream& ostr, const MultiLockSafe& comboLock);


private:
	vector<CombinationLock> locks;

	Dials root;

	DialRotations UHF;
	DialRotations LHF;
	DialRotations PHF;

	SafeSettings& safeSettings = SafeSettings::getInstance();
};

