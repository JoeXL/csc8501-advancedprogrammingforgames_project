#pragma once
#include "DialRotations.h"
#include <iostream>
#include "SafeSettings.h"
#include <string>

using namespace std;

class Dials
{
public:
	Dials();
	Dials(const Dials& other);
	Dials(int x, int y, int z, int w);
	~Dials();

	void setDials(int x, int y, int z, int w);

	const bool& isDialValid() const;

	friend Dials operator+(const Dials& lhs, const DialRotations& rhs);

	/* Returns the difference between two Dials as a DialRotations */
	friend DialRotations operator-(const Dials& lhs, const Dials& rhs);

	Dials& operator+=(const DialRotations& rhs);

	Dials& operator=(const Dials& other);

	const bool& operator==(const Dials& rhs) const;
	
	const int& operator[](int i) const;

	const bool& operator<(const Dials& rhs) const;

	friend ostream& operator<<(ostream& ostr, const Dials& dial);

private:
	void alignValuesIntoRange();

	int x;
	int y;
	int z;
	int w;

	SafeSettings& dialSettings = SafeSettings::getInstance();
};

