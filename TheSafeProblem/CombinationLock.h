#pragma once
#include <vector>
#include <iostream>
#include <string>
#include "Dials.h"

using namespace std;

class CombinationLock
{
public:
	CombinationLock();
	CombinationLock(const CombinationLock& other);
	~CombinationLock();

	void rotateNumbers(const DialRotations& rot);

	const Dials& getCurrentNumber() const;

	void setCurrentNumber(const Dials& dials);

	/* If code is correct, open, else stay locked */
	bool pressButton();

	/* Resets the currentNumber to the lockNumber */
	void resetLock();

	const Dials& setupLock(const Dials& root, const DialRotations& UHF, const DialRotations& LHF, const DialRotations& PHF); //if the lock is unlocked, generate the CN, LN and HN. Returns the HN

	bool isLockValid();

	void setLockIndex(int i) {
		lockIndex = i;
	}

	CombinationLock& operator=(const CombinationLock& other);

	friend ostream& operator<<(ostream& ostr, const CombinationLock& comboLock);

private:
	Dials hashNumber; //the number that is passed to the next lock
	Dials correctNumber; //the number that will unlock the lock
	Dials lockNumber; //the number that is initially displayed when the lock is locked
	Dials currentNumber; //the number that is currently displayed by the lock

	int lockIndex;

	bool isLocked;
};