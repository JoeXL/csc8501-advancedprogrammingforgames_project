// TheSafeProblem.cpp : Defines the entry point for the console application.
//
#include "pch.h"
#include "stdafx.h"
#include <iostream>;
#include <random>
#include <time.h>
#include <exception>
#include <fstream>

#include "MultiLockSafe.h"

using namespace std;

static DialRotations UHFValues;
static DialRotations LHFValues;
static DialRotations PHFValues;

string keyFileName = "keyFile";
string multiSafeFileName = "multiSafeFile";
string lockedSafeFileName = "lockedSafeFile";

vector<MultiLockSafe> safes;
vector<MultiLockSafe> validSafes;

SafeSettings& safeSettings = SafeSettings::getInstance();

Dials genRoot() {
	Dials root;
	int x = safeSettings.getMinDial() + rand() % (safeSettings.getMaxDial() + 1);
	int y = safeSettings.getMinDial() + rand() % (safeSettings.getMaxDial() + 1);
	int z = safeSettings.getMinDial() + rand() % (safeSettings.getMaxDial() + 1);
	int w = safeSettings.getMinDial() + rand() % (safeSettings.getMaxDial() + 1);
	root.setDials(x, y, z, w);
	return root;
}

void genHashFunctions() {
	for (int i = 0; i < 3; ++i) {
		int x = (rand() % (safeSettings.getMaxRot() * 2)) - safeSettings.getMaxRot();
		int y = (rand() % (safeSettings.getMaxRot() * 2)) - safeSettings.getMaxRot();
		int z = (rand() % (safeSettings.getMaxRot() * 2)) - safeSettings.getMaxRot();
		int w = (rand() % (safeSettings.getMaxRot() * 2)) - safeSettings.getMaxRot();
		if (i == 0)
			UHFValues.setRotations(x, y, z, w);
		else if (i == 1)
			LHFValues.setRotations(x, y, z, w);
		else
			PHFValues.setRotations(x, y, z, w);
	}
}

void readSettingsFile() {
	string line;
	ifstream settingsFile("settings.txt");
	if (settingsFile.is_open()) {
		while (getline(settingsFile, line)) {
			if (line.find("Number of safes = ") != string::npos) {
				line = line.substr(line.find("= ") + 2);
				try {
					int numSafes = stoi(line);
					if (numSafes > 0 && numSafes <= 2000)
						safeSettings.setNumSafes(numSafes);
					else
						cout << "Number of safes in settings file must be greater than 0 and less than or equal to 2000." << endl << "Using default value of " << safeSettings.getNumSafes() << endl;
				}
				catch (invalid_argument iae) {
					cout << "Number of safes in settings file was invalid.\n" <<
						"Using default value of " << safeSettings.getNumSafes() << endl;
				}
			}
			else if (line.find("Number of locks per safe = ") != string::npos) {
				line = line.substr(line.find("= ") + 2);
				try {
					int numLocks = stoi(line);
					if (numLocks > 0)
						safeSettings.setNumLocks(numLocks);
					else
						cout << "Number of locks in settings file must be greater than 0." << endl << "Using default value of " << safeSettings.getNumLocks() << endl;
				}
				catch (invalid_argument iae) {
					cout << "Number of locks in the settings file was invalid.\n" <<
						"Using default value of " << safeSettings.getNumLocks() << endl;
				}
			}
		}
	}
	settingsFile.close();
}

void generateKeyFile() {
	ofstream keyFile(keyFileName + ".txt");

	if (keyFile.is_open()) {
		keyFile << "NS " << safeSettings.getNumSafes() << endl;

		genHashFunctions();

		vector<Dials> generatedRoots;

		for (int i = 0; i < safeSettings.getNumSafes(); ++i) {
			Dials root;
			while (true) {
				root = genRoot();
				bool isNewRoot = true;
				for (int i = 0; i < generatedRoots.size(); ++i) {
					if (generatedRoots.at(i) == root)
						isNewRoot = false;
				}
				if (isNewRoot)
					break;
			}
			keyFile << "ROOT " << root << endl;
			keyFile << "UHF " << UHFValues << endl;
			keyFile << "LHF " << LHFValues << endl;
			keyFile << "PHF " << PHFValues << endl;
		}
	}

	keyFile.close();

}

void readKeyFile() {
	string line;
	ifstream keyFile(keyFileName + ".txt");
	if (keyFile.is_open()) {
		while (getline(keyFile, line)) {
			if (line.find("NS") != string::npos) {
				line = line.substr(line.find("NS ") + 3);
				int numSolutions = stoi(line);
				getline(keyFile, line);
			}
			if (line.find("ROOT") != string::npos) {
				MultiLockSafe multiSafe;
				line = line.substr(line.find("ROOT ") + 5);
				Dials root;
				root.setDials(line[0] - '0', line[1] - '0', line[2] - '0', line[3] - '0');
				multiSafe.setRoot(root);
				getline(keyFile, line);
				if (line.find("UHF") != string::npos) {
					line = line.substr(line.find("UHF ") + 4);
					DialRotations uhf;
					uhf.setRotations(stoi(line.substr(0, 2)), stoi(line.substr(3, 2)), stoi(line.substr(6, 2)), stoi(line.substr(9, 2)));
					multiSafe.setUHF(uhf);
					getline(keyFile, line);
				}
				if (line.find("LHF") != string::npos) {
					line = line.substr(line.find("LHF ") + 4);
					DialRotations lhf;
					lhf.setRotations(stoi(line.substr(0, 2)), stoi(line.substr(3, 2)), stoi(line.substr(6, 2)), stoi(line.substr(9, 2)));
					multiSafe.setLHF(lhf);
					getline(keyFile, line);
				}
				if (line.find("PHF") != string::npos) {
					line = line.substr(line.find("PHF ") + 4);
					DialRotations phf;
					phf.setRotations(stoi(line.substr(0, 2)), stoi(line.substr(3, 2)), stoi(line.substr(6, 2)), stoi(line.substr(9, 2)));
					multiSafe.setPHF(phf);
				}
				safes.push_back(multiSafe);
			}
		}
	}
	keyFile.close();
}

void generateMultiSafeFile() {
	ofstream multiSafeFile(multiSafeFileName + ".txt");

	if (multiSafeFile.is_open()) {
		for (int i = 0; i < safes.size(); ++i) {
			multiSafeFile << "NS " << i << " ";
			safes.at(i).processSafe();
			MultiLockSafe newSafe(safes.at(i));
			bool b = newSafe.isSafeValid();
			if (b) {
				validSafes.push_back(safes.at(i));
			}
			else {
				multiSafeFile << "NOT ";
			}
			multiSafeFile << "VALID" << endl;
			multiSafeFile << safes.at(i) << endl;
		}
	}
	multiSafeFile.close();
}

void generateLockedSafeFile() {
	ofstream lockedSafeFile(lockedSafeFileName + ".txt");

	if (lockedSafeFile.is_open()) {
		lockedSafeFile << "NL " << validSafes.size() << endl;
		for (int i = 0; i < validSafes.size(); ++i) {
			lockedSafeFile << "ROOT: " << validSafes.at(i).getRoot() << endl;
			for (int j = 0; j < safeSettings.getNumLocks(); ++j) {
				lockedSafeFile << "LN" << j << ": " << validSafes.at(i).getLock(j).getCurrentNumber() << endl;
			}
			lockedSafeFile << endl;

		}
	}
	lockedSafeFile.close();
}

bool isValidFilename(string filename) {
	if (filename.find('\\') == string::npos &&
		filename.find('/') == string::npos &&
		filename.find('<') == string::npos &&
		filename.find('>') == string::npos &&
		filename.find('|') == string::npos &&
		filename.find('"') == string::npos &&
		filename.find(':') == string::npos &&
		filename.find('?') == string::npos &&
		filename.find('*') == string::npos)
		return true;
	return false;
}

int main()
{
	/* Start user input */

	cout << "Enter a blank filename to use default values." << endl;
	while (true) {
		string tempFilename = "";
		cout << "Enter a filename for the locked key file: ";
		getline(cin, tempFilename);
		if (isValidFilename(tempFilename)) {
			if (tempFilename != "")
				keyFileName = tempFilename;
			break;
		}
		cout << "Invalid filename!" << endl;
	}

	while (true) {
		string tempFilename = "";
		cout << "Enter a filename for the multi safe file: ";
		getline(cin, tempFilename);
		if (isValidFilename(tempFilename) && tempFilename != keyFileName) {
			if (tempFilename != "")
				multiSafeFileName = tempFilename;
			break;
		}
		cout << "Invalid filename!" << endl;
	}

	while (true) {
		string tempFilename = "";
		cout << "Enter a filename for the locked safe file: ";
		getline(cin, tempFilename);
		if (isValidFilename(tempFilename) && tempFilename != keyFileName && tempFilename != multiSafeFileName) {
			if (tempFilename != "")
				lockedSafeFileName = tempFilename;
			break;
		}
		cout << "Invalid filename!" << endl;
	}

	/* Generate and process safe data */

	srand(time(NULL));
	readSettingsFile();
	genHashFunctions();
	generateKeyFile();
	readKeyFile();
	generateMultiSafeFile();
	generateLockedSafeFile();

	/* Print total valid safes */

	int count = 0;
	int validCount = 0;

	for (MultiLockSafe safe : safes) {
		if (safe.isSafeValid()) {
			validCount++;
		}
		count++;
	}

	cout << "Total safes generated: " << count << endl;
	cout << "Number of valid safes: " << validCount << endl;

	system("pause");

	return 0;
}